from django.urls import path
from accounts.views import user_signup, login_view, user_logout


urlpatterns = [
    path("login/", login_view, name="login"),
    path("signup/", user_signup, name="signup"),
    path("logout/", user_logout, name="logout"),
]
